using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickup : MonoBehaviour
{
    [SerializeField] AudioClip coinPickupSFX;
    [SerializeField] int pointsForCoinPickup = 100;
    bool wasCollected;
    void OnTriggerEnter2D(Collider2D other) {
        if(other.tag =="Player" && !wasCollected){
            wasCollected=true;
            AudioSource.PlayClipAtPoint(coinPickupSFX,Camera.main.transform.position);
            FindObjectOfType<GameSession>().AddScore(pointsForCoinPickup);
            Destroy(gameObject);
        }
    }
}
