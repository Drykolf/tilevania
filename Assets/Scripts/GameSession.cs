using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSession : MonoBehaviour
{
    [SerializeField] int playerLives = 3;
    [SerializeField] TextMeshProUGUI livesText;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] int score = 0;

    void Awake() {
        int numGameSessions = FindObjectsOfType<GameSession>().Length;
        if(numGameSessions>1)Destroy(gameObject);
        else{DontDestroyOnLoad(gameObject);}
    }

    void Start() {
        livesText.text = playerLives.ToString();  
        scoreText.text = score.ToString();  
    }

    public void AddScore(int pointsToAdd){
        score+=pointsToAdd;
        scoreText.text = score.ToString();
    }

    public void ProcessPlayerDeath(){
        if(playerLives>1){
            RemoveLife();
        }
        else{
            ResetGameSession();
        }
    }

    void RemoveLife()
    {
        playerLives -=1;
        livesText.text = playerLives.ToString();
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);    
    }

    void ResetGameSession(){
        FindObjectOfType<ScenePersist>().ResetScenePersist();
        SceneManager.LoadScene(0);
        Destroy(gameObject);
    }
}
